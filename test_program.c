#include "tree_based_quadrants.h"

int main () {
	quadrant_t q, ld, anc;
	
	root (&q);
	printf ("q.x = %i, q.y = %i, q.z = %i, level = %i\n", q.x, q.y, q.z, q.level);
	last_descendant (&q, &ld, 2);
	printf ("ld.x = %i, ld.y = %i, ld.z = %i, level = %i\n", ld.x, ld.y, ld.z, ld.level);
	ancestor (&ld, 0, &anc);
	printf ("anc.x = %i, anc.y = %i, anc.z = %i, level = %i\n", anc.x, anc.y, anc.z, anc.level);
	printf ("%i\n", is_ancestor(&q,&ld));
	printf ("%i\n", is_ancestor(&ld,&q));
	return 0;
}
