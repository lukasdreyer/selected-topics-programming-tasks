#include "tree_based_quadrants.h"

void
root (quadrant_t * q)
{
  q->level = 0;
  q->x = q->y = q->z = 0;
}

void
ancestor (const quadrant_t * q, int level,quadrant_t * ancestor_quadrant)
{
  qcoord_t        mask;
#if 0
  if (!is_valid (q)) {
    printf ("ancestor: the input quadrant has to be valid.\n");
    return;
  }
#endif

  if((int) q->level < level || level < 0) {
    printf("ancestor: level should be between 0 and quadrant level");
    return;
  }
  mask = ~(QUADRANT_LEN(level) - 1);

  ancestor_quadrant->level = level;
  ancestor_quadrant->x = q->x & mask;
  ancestor_quadrant->y = q->y & mask;
  ancestor_quadrant->z = q->z & mask;
}

int
is_ancestor (const quadrant_t * q, quadrant_t * r)
{
  qcoord_t          mask;
#if 0
  if (!is_valid (q) || !is_valid (r)) {
    printf ("is_ancestor: the input quadrants have to be valid.\n");
    return -1;
  }
#endif
  mask = ~(QUADRANT_LEN(q->level) - 1);
  return (q->level <= r->level) && !((q->x ^ r->x) & mask)
                                && !((q->y ^ r->y) & mask)
                                && !((q->z ^ r->z) & mask);
}


void
last_descendant (const quadrant_t * q, quadrant_t * last_descendant,
                 int level)
{
  qcoord_t            shift;

#if 0
  if (!is_valid (q)) {
    printf ("last_descendant: the input quadrant has to be valid.\n");
    return;
  }
#endif
  if ((int) q->level > level && level >= MAXLEVEL) {
    printf
      ("last_descendant: level should lie between quadrant level (included) \
        and MAXLEVEL (excluded).\n");
    return;
  }

  shift = QUADRANT_LEN (q->level) - QUADRANT_LEN (level);

  last_descendant->x = q->x + shift;
  last_descendant->y = q->y + shift;
  last_descendant->z = q->z + shift;

  last_descendant->level = (int8_t) level;
}
